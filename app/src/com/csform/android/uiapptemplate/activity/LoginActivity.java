package com.csform.android.uiapptemplate.activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.csform.android.uiapptemplate.R;
import com.csform.android.uiapptemplate.fragment.LoginFragment;

public class LoginActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportFragmentManager().beginTransaction().add(R.id.containerLogin, LoginFragment.newInstance("", "")).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
