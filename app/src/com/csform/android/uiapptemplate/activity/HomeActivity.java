package com.csform.android.uiapptemplate.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.csform.android.uiapptemplate.R;
import com.csform.android.uiapptemplate.fragment.LoginFragment;

public class HomeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
    }

}
