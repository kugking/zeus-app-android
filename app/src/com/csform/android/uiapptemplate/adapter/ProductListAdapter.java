package com.csform.android.uiapptemplate.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate.R;
import com.csform.android.uiapptemplate.view.AnimatedExpandableListView;

import java.util.ArrayList;

/**
 * Created by kugking on 4/5/16 AD.
 */
public class ProductListAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
    Activity activity;

    public ProductListAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return 0;
    }

    @Override
    public int getGroupCount() {
        return 10;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View button = View.inflate(activity, R.layout.list_item_google_cards_shop, null);
        return button;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
