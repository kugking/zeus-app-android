package com.csform.android.uiapptemplate.fragment_template;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.csform.android.uiapptemplate.activity_template.ParallaxActivity;
import com.csform.android.uiapptemplate.activity_template.ParallaxKenBurnsActivity;
import com.csform.android.uiapptemplate.activity_template.ParallaxMediaActivity;
import com.csform.android.uiapptemplate.activity_template.ParallaxShopActivity;
import com.csform.android.uiapptemplate.activity_template.ParallaxSocialActivity;
import com.csform.android.uiapptemplate.activity_template.ParallaxTravelActivity;
import com.csform.android.uiapptemplate.R;
import com.csform.android.uiapptemplate.adapter_template.SubcategoryAdapter;

public class ParallaxEffectsFragment extends Fragment implements
		OnItemClickListener {

	private ListView mListView;
	private List<String> mParallaxEffects;

	public static ParallaxEffectsFragment newInstance() {
		return new ParallaxEffectsFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mParallaxEffects = new ArrayList<String>();
		mParallaxEffects.add(ParallaxKenBurnsActivity.TAG);
		mParallaxEffects.add(ParallaxActivity.TAG);
		mParallaxEffects.add(ParallaxMediaActivity.TAG);
		mParallaxEffects.add(ParallaxShopActivity.TAG);
		mParallaxEffects.add(ParallaxSocialActivity.TAG);
		mParallaxEffects.add(ParallaxTravelActivity.TAG);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_paralax, container,
				false);

		mListView = (ListView) rootView.findViewById(R.id.list_view);
		mListView.setAdapter(new SubcategoryAdapter(getActivity(),
				mParallaxEffects));
		mListView.setOnItemClickListener(this);

		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		String pe = mParallaxEffects.get(position);
		Intent intent = null;
		if (pe.equals(ParallaxKenBurnsActivity.TAG)) {
			intent = new Intent(getActivity(), ParallaxKenBurnsActivity.class);
		} else if (pe.equals(ParallaxActivity.TAG)) {
			intent = new Intent(getActivity(), ParallaxActivity.class);
		} else if (pe.equals(ParallaxTravelActivity.TAG)) {
			intent = new Intent(getActivity(), ParallaxTravelActivity.class);
		} else if (pe.equals(ParallaxSocialActivity.TAG)) {
			intent = new Intent(getActivity(), ParallaxSocialActivity.class);
		} else if (pe.equals(ParallaxShopActivity.TAG)) {
			intent = new Intent(getActivity(), ParallaxShopActivity.class);
		} else if (pe.equals(ParallaxMediaActivity.TAG)) {
			intent = new Intent(getActivity(), ParallaxMediaActivity.class);
		}
		if (intent != null)
			startActivity(intent);
	}
}
